'use strict';

// mobile menu
(() => {
	const menu = document.querySelector('.js-menu');
	const menuBtn = document.querySelector('.js-menuBtn');

	menuBtn.addEventListener('click', ev => {
		const target = ev.target || ev.currentTarget;

		if (target.classList.contains('active')) {
			target.classList.remove('active');
			menu.classList.remove('active');
			target.classList.add('close');
			document.documentElement.classList.remove('no-scroll');
			setTimeout(() => {
				target.classList.remove('close');
			}, 500)
		} else {
			target.classList.add('active');
			menu.classList.add('active');
			document.documentElement.classList.add('no-scroll');
		}
	});

	const menuLinks = document.querySelectorAll('.header__nav-link');

	Array.prototype.forEach.call(menuLinks, el => {
		el.addEventListener('click', () => {
			menuBtn.classList.remove('active');
			menu.classList.remove('active');
			menuBtn.classList.add('close');
			document.documentElement.classList.remove('no-scroll');
		})
	})


})();

// video player
(() => {
	const video = document.querySelector('.js-video');
	const videoBtn = document.querySelector('.js-videoBtn');
	const videoOverlay = document.querySelector('.js-videoOverlay');

	videoBtn.addEventListener('click', ev => {
		ev.preventDefault();
		videoOverlay.setAttribute('data-show', 'false');
		setTimeout(() => {
			video.play()
		}, 700)
	});

	video.addEventListener('pause', () => {
		videoOverlay.setAttribute('data-show', 'true');
	})

})();

// custom select
(() => {

	class Select {
		constructor (container) {

			this.input = container.querySelector('input');
			this.dropList = container.querySelector('.js-dropList');
			this.dropListItems = this.dropList.querySelectorAll('li');
			this.valueContainer = container.querySelector('.js-value');
						
			this.valueContainer.addEventListener('click', () => this.toggleList());

			Array.prototype.forEach.call(this.dropListItems, item => {
				item.addEventListener('click', (event) => this.selectItem(event))
			});

			window.addEventListener('click', event => {
				const target = event.target || event.currentTarget;
				if (target !== this.valueContainer)
					this.dropList.classList.remove('active')
			})

		}

		toggleList() {
			this.dropList.classList.toggle('active');
		}

		selectItem(event) {
			const target = event.target || event.currentTarget;
			const valueInput = target.innerHTML;
			this.input.value = valueInput;
			this.valueContainer.innerHTML = valueInput;
			this.toggleList();
		}

	}

	const selects = document.querySelectorAll('.js-select');

	Array.from(selects).forEach(element => new Select(element))

})();

// telegram message
(() => {
	const crossDomainPost = (param) => {

		const chatBot = 383655115;
		// chatBot - @byGats_bot
		// start this bot

		const chatId = 380460973;
		//chatid - @userinfobot
		//start this bot,  and copy value id

		// Add the iframe with a unique name
		const iframe = document.createElement("iframe");
		const uniqueString = "CHANGE_THIS_TO_SOME_UNIQUE_STRING";
		document.body.appendChild(iframe);
		iframe.style.display = "none";
		iframe.contentWindow.name = uniqueString;

		// construct a form with hidden inputs, targeting the iframe
		const form = document.createElement("form");
		form.target = uniqueString;


		if (param === 'touch') {
			const subject = 'Touch';
			const field1 = 'Name:';
			const field2 = 'Email:';
			const field3 = 'Country:';
			const field4 = 'City:';
			const field5 = 'Message:';
			form.action =
				"https://api.telegram.org/bot" + chatBot + ":AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=" + chatId + "&text=" + subject + "%0A" + field1 + " " + document.querySelector('input[name="nameTouch"]').value + "%0A" + field2 + " " + document.querySelector('input[name="emailTouch"]').value  + "%0A" + field3 + " " + document.querySelector('input[name="countryTouch"]').value  + "%0A" + field4 + " " + document.querySelector('input[name="cityTouch"]').value  + "%0A" + field5 + " " + document.querySelector('textarea[name="messageTouch"]').value;
		}


		form.method = "POST";

		// repeat for each parameter
		const input = document.createElement("input");
		input.type = "hidden";
		input.name = "INSERT_YOUR_PARAMETER_NAME_HERE";
		input.value = "INSERT_YOUR_PARAMETER_VALUE_HERE";
		form.appendChild(input);

		document.body.appendChild(form);
		form.submit();
	};

//touch form submit

	const touchForm = document.querySelector('.js-touchForm');
	const touchFormBtn = document.querySelector('.js-touchSbm');

	touchFormBtn.addEventListener('click', ev => {
		ev.preventDefault();
		const inputs = touchForm.querySelectorAll('input');
		const emailInput = touchForm.querySelector('input[name="emailTouch"]');
		const re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		const validEmail = re.test(emailInput.value);
		const completed = touchForm.querySelectorAll('.js-completed');

		if (
			[...inputs].some(input => input.value.length > 1) && validEmail
		) {
			crossDomainPost('touch');
			touchForm.reset();
			Array.from(completed).forEach(element => {
				const parent = element.parentNode;
				const span = parent.querySelector('span');
				span.classList.remove('active');
			});
			touchFormBtn.innerHTML = 'Done!';
			touchFormBtn.classList.add('done');
			setTimeout(() => {
				touchFormBtn.innerHTML = 'Apply!';
				touchFormBtn.classList.remove('done');
			}, 3000)

		} else {
			touchFormBtn.innerHTML = 'Error!';
			touchFormBtn.classList.add('done');
			setTimeout(() => {
				touchFormBtn.innerHTML = 'Send Message!';
				touchFormBtn.classList.remove('done');
			}, 3000)
		}
	})

})();

// scroll to
(() => {

	const links = document.querySelectorAll('.js-scrollTo');

	Array.prototype.forEach.call(links, link => {
		link.addEventListener('click', ev => {
			ev.preventDefault();
			const target = ev.target || ev.currentTarget;
			const href = target.getAttribute('href');
			const elementToScroll = document.querySelector(href);
			const offset = elementToScroll.getBoundingClientRect().y;
			window.scrollBy({
				top: offset,
				left: 0,
				behavior: 'smooth'
			})
		})
	})

})();

// completed input
(() => {

	const inputs = document.querySelectorAll('.js-completed');

	Array.prototype.forEach.call(inputs, input => {
		input.addEventListener('input', ev => {
			const target = ev.target || ev.currentTarget;
			const parent = target.parentNode;
			const span = parent.querySelector('span');

			if (target.value.length > 0) {
				span.classList.add('active')
			} else {
				span.classList.remove('active')
			}
		})
	})

})();