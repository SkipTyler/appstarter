> AppStarter test project

## Installation

1. `git clone https://gitlabcom/SkipTyler/appstarter.git`
2. `cd appstarter/`
3. `npm i`
4.  - `npm run dev` - it runs webpack-dev-server with browser-sync support and builds source-maps
    - `npm run build` - it minifies js, css, images and copy others files (fonts, video) 
    
    _(At the moment, the assembly is working with an error, it does not correctly describe the image paths in the html file. Fix during)_
    


#### To view the compiled version, you need a local server
- `npm i serve`
- `serve -s dist`
    
## Dependencies

* node v10.9.0
* npm 6.4.1
___


The site has a feedback form. The address to send is in app.js (113 line)